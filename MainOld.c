/*
    Este código tem como objetivo usar o TIM4 como contador
 entre um espaço de 2 eventos, o inicial e o final.
 
 */
//-------------------- Tipos de dados --------------------  
#define __IO volatile  //Habilita RW
#define uint32_t unsigned int
#define u32 unsigned int

#define uint8_t unsigned char
#define u8 unsigned char

#define int32_t int

#define uint16_t unsigned short
#define u16 unsigned short

#define STACKINT	0x20000000

/* *
 * Definições de macros de interrupções NVIC.
 * */

#define NVIC_BASE (( __IO u32 *) 0xE000E100) 

#define NVIC_ISER		(( (__IO u32 * ) NVIC_BASE + 0x00 ))
#define NVIC_ICER		(( (__IO u32 * ) NVIC_BASE + 0x80 ))
#define NVIC_ISPR		(( (__IO u32 * ) NVIC_BASE + 0x100 ))
#define NVIC_ICPR		(( (__IO u32 * ) NVIC_BASE + 0x180 ))
#define NVIC_IABR		(( (__IO u32 * ) NVIC_BASE + 0x200 ))
#define NVIC_IPR		(( (__IO u32 * ) NVIC_BASE + 0x300 ))


//#define NVIC (* ( NVIC_BASE + 0x058) )

//--------------------  GPIOx Def --------------------
#define GPIOA ((GPIO_TypeDef * ) 0x40010800) /* GPIOx Addr Base */
#define GPIOB ((GPIO_TypeDef * ) 0x40010c00) 
#define GPIOC ((GPIO_TypeDef * ) 0x40011000) 
#define GPIOD ((GPIO_TypeDef * ) 0x40011400) 
#define GPIOE ((GPIO_TypeDef * ) 0x40011800) 
#define GPIOF ((GPIO_TypeDef * ) 0x40011c00) 
#define GPIOG ((GPIO_TypeDef * ) 0x40012000) 

//-------------------- TIMx Def -------------------- 
#define TIM1 ((TIM_TypeDef * ) 0x40012c00) /* TIMx Addr Base */
#define TIM2 ((TIM_TypeDef * ) 0x40000000) 
#define TIM3 ((TIM_TypeDef * ) 0x40000400) 
#define TIM4 ((TIM_TypeDef * ) 0x40000800) 
#define TIM5 ((TIM_TypeDef * ) 0x40000c00) 
#define TIM6 ((TIM_TypeDef * ) 0x40001000) 
#define TIM7 ((TIM_TypeDef * ) 0x40001400) 
#define TIM8 ((TIM_TypeDef * ) 0x40013400) 
#define TIM9 ((TIM_TypeDef * ) 0x40014C00) 
#define TIM10 ((TIM_TypeDef * ) 0x40015000) 
#define TIM11 ((TIM_TypeDef * ) 0x40015400) 
#define TIM12 ((TIM_TypeDef * ) 0x40001800) 
#define TIM13 ((TIM_TypeDef * ) 0x40001c00) 
#define TIM14 ((TIM_TypeDef * ) 0x40002000) 
#define FLASH (( FLASH_TypeDef * ) 0x40022000)

//-------------------- AFIO 
#define AFIO (( AFIO_TypeDef *) 0x40010000 )

//-------------------- EXTI 
#define EXTI (( EXTI_TypeDef *) 0x40010400 )
#define EXTI0 (( __IO u32 *) 0x00000058)


//-------------------- RCC Def -------------------- 
#define RCC (( RCC_TypeDef *) 0x40021000) /* RCC Addr Base */

//-------------------- OTG_FS_Global Def -------------------- 
/* OTG_FS_Global_BASE (__IO *) 0x50000000 // 
 * Endereço base obtido em: 
 * https://stm32-rs.github.io/stm32-rs/STM32F103.html#OTG_FS_GLOBAL
*/

/*-------------------- NVIC -------------------- 
/ Macro para habilitar interrupções.
*/
#define EN_NVIC(address)	(*(volatile uint32_t *) (NVIC_ISER + 0x40 + ((position) * 4 ))) 
// https://developer.arm.com/documentation/dui0662/b/Cortex-M0--Peripherals/Nested-Vectored-Interrupt-Controller/Interrupt-Set-Enable-Register

//-------------------- Variáveis globais para a função "Botao"

__IO u8 larg_pulso      = 0;
__IO u8 ult_cap         = 0;
__IO u8 polar_sinal    = 0;

//-------------------- Area de funções ---------------  

typedef struct{
	__IO u32 CRL ;		// 0x00 MODO de operação Port configuration register low
	__IO u32 CRH ;		// 0x04 Port configuration register high
	__IO u32 IDR ;		// 0x08 Port input data register
	__IO u32 ODR ;		// 0x0c Port output data register
	__IO u32 BSRR ;	// 0x10 Port bit set/reset register
	__IO u32 BRR ;		// 0x14 Port bit reset register
	__IO u32 LCKR ;	// 0x18 Port configuration lock register
} GPIO_TypeDef ;

typedef struct{
	__IO u32 EVCR;		// 0x00
	__IO u32 MAPR;		// 0x04
	__IO u16 EXTICR1; 	// 0X08
	__IO u16 RES1 ; 	// 
	__IO u16 EXTICR2; 	// 0x0c
	__IO u16 RES2 ; 	// 
	__IO u16 EXTICR3; 	// 0x10
	__IO u16 RES3 ; 	//
	__IO u16 EXTICR4; 	// 0x14
	__IO u16 RES4 ; 	// 
	__IO u32 reservado;	// 0x18
	__IO u32 MAPR2;		// 0x1c
} AFIO_TypeDef ;

typedef struct{
	__IO u32 CR1; 		// 0x00
	__IO u32 CR2;		// 0x04
	__IO u32 SMCR;		// 0x08
	__IO u32 DIER;		// 0x0c
	__IO u32 SR; 		// 0x10
	__IO u32 EGR; 		// 0x14
	__IO u32 CCMR1;		// 0x18 ->  input/output capture mode
	__IO u32 CCMR2;		// 0x1c ->  input/output capture mode
	__IO u32 CCER; 		// 0x20 <--- 
	__IO u32 CNT; 		// 0x24
	__IO u32 PSC;		// 0x28
	__IO u32 ARR;		// 0x2c
	__IO u32 RCR;		// 0x30
	__IO u32 CCR1; 		// 0x34
	__IO u32 CCR2;		// 0x38 
	__IO u32 CCR3;		// 0x3c
	__IO u32 CCR4;		// 0x40
	__IO u32 BDTR;		// 0x44 -> Descritivo na pagina 344 do RM0008
	__IO u32 DCR;		// 0x48
	__IO u32 DMAR;		// 0x4c
} TIM_TypeDef ;

typedef struct{
//		       		  Offset
	__IO u32 CR; 		// 0x00
	__IO u32 CFGR;		// 0x04 
	__IO u32 CIR;		// 0x08
	__IO u32 APB2RSTR;	// 0x0c
	__IO u32 APB1RSTR;	// 0x10
	__IO u32 AHBENR;	// 0x14
	__IO u32 APB2ENR;	// 0x18
	__IO u32 APB1ENR;	// 0x1c
	__IO u32 BDCR;		// 0x20
	__IO u32 CSR;		// 0x24
	__IO u32 AHBRSTR;	// 0x28
	__IO u32 CFGR2;		// 0x2c
} RCC_TypeDef;

typedef struct{
	__IO u32 GOTGCTL; 	// 0x00
   __IO u32 GOTGINT;		// 0x04
   __IO u32 GAHBCFG;		/* 0x08 <<- Responsavel por habilitar 
								 interrupções AHB ou USB */
   __IO u32 GUSBCFG;		// 0x0c
  	__IO u32 GRSTCTL;		// 0x10
  	__IO u32 GINTSTS;		// 0x14
  	__IO u32 GINTMSK;		// 0x18
  	__IO uint16_t GRXSTSR_1_H;	// 0x1c Host/Device Mode
  	__IO uint16_t GRXSTSR_1_D;	// 0x1c Host/Device Mode
  	__IO uint16_t GRXSTSR_2_H;	// 0x20 Host/Device Mode
  	__IO uint16_t GRXSTSRPR_2_D;	// 0x20 Host/Device Mode
   __IO u32 GRXFSIZ;		// 0x24
	__IO uint16_t  HNPTXFSIZ;	// 0x28 
	__IO uint16_t  DIEPTXF0;	// 0x28
	__IO u32 HNPTXSTS;		// 0x2c
} OTG_FS_TypeDef;

typedef struct {
	__IO u32 IMR ;		// 0x00
	__IO u32 EMR ;		// 0x04	
	__IO u32 RTSR ;		// 0x08	
	__IO u32 FTSR ;		// 0x0c	
	__IO u32 SWIER ;	// 0x10	
	__IO u32 PR ;		// 0x14	
} EXTI_TypeDef;

void SystemInit (void) {
  RCC->CR |= (uint32_t)0x00000001;
  RCC->CFGR &= (uint32_t)0xF8FF0000;
  RCC->CR &= (uint32_t)0xFEF6FFFF;
  RCC->CR &= (uint32_t)0xFFFBFFFF;
  RCC->CFGR &= (uint32_t)0xFF80FFFF;
  RCC->CIR = 0x009F0000;
 }

void set_system_clock_to_25Mhz (void){
	RCC->CR 	|= 1<<16 ;
	while (!( RCC->CR & (1<<17) ));	
	RCC->CFGR 	|= 1<<0;
}

void Botao(){
    /* Função de chamada do TIM4 para calcular intervalo de
     * tempo entre 2 eventos.
     */
			  GPIOC->ODR	 ^= 1<<13;
     u16 cap_atual;
     
     if (TIM4->SR & ( 1<<1 )){ /* bit 1 ref = CC1F do canal 1 TI1
                                    SR = Registrador de status de interrupção 
                                    NVIC */
         cap_atual = TIM4->CCR1 ; /* Registrador de 16 bits
                                    Valor contado transferido pela a última 
                                    captura de evento (IC1)
                                    Limpa automaticamente o SR_CC1F */
                                    
        polar_sinal ^= 1 ; /* Troca a flag de polaridade */
        
        if (polar_sinal == 0 ) // == 0
            larg_pulso = cap_atual - ult_cap ; 
        
        ult_cap = cap_atual;
    }
    
    if (TIM4->SR & ( 1 << 0 ))
       TIM4->SR &= ~ ( 1<<0); /* Limpa a flag geral de interrupção, UIF  */ 
         
}

void TIM4_IRQHandler (){
		  Botao(); }


void en_nvic ( u8 position) { /* Habilita a Interrupção "position" 
													 Pag:201 RM0008 */
	EN_NVIC( position ) = 1; }


int32_t main (void) {
	/* Input capture, contando o intervalo entre interrupções
	 * TIM4 -> ch1_PB6
	 * */
	set_system_clock_to_25Mhz();

	RCC->APB2ENR |= 0x3 << 3 | 1<<0 ; /* GPIO BC e AFIO */
	RCC->APB1ENR |= 1<<2;
//-----------------------------------
	GPIOC->CRH	 |= 0x03 << 20;
	GPIOC->ODR	 ^= 1<<13;
//-----------------------------------

// TIM4 -> Seleção de pino e mapeamento

	GPIOB->CRL |= 0x8 << 24; /* Input pin PB6 */
	TIM4->CCMR1 |= 0b01 << 0 ; /* 01 _ IC1 is mapped on TI1
					Bits 1:0 CC1S*/

	TIM4->CCMR1 |= 0b0011 << 4; /*Bits 7:4 IC1F: Input capture 1 filter
											Define o filtro de amostras iniciais
											0b0011 → 8 sinais de clock para validação */
	TIM4->CCER &= ~(1<<1); /* CC1P -> para input, seleciona a 
									  borda de transição (subida=0 ou decida=1) */

	TIM4->CCMR1 &= ~( 0b11<< 2); /* Bits 3:2 IC1PSC: Input capture 1 prescaler
											 00: no prescaler -> Define o numero de evento 
											 para ativar o evento */
	TIM4->CCER |= ( 1 << 0 ); /* Bit 0 CC1E: Capture/Compare 1 output enable
											Habilita a captura de eventos.*/
	TIM4->DIER |= (1 << 1);  /* Bit 1 CC1IE: Capture/Compare 1 interrupt enable
										 Habilita a interrupção. */
	TIM4->CR1 |= ( 1 << 0 ); /* Bit 0 CEN: Counter enable */
	


	en_nvic( 30 ); /* Vector table TIM4 -> position 30 -> 0xB8 **/


	while (1);
	return 0;
//o // de proposito
}

/* Definições de bits "CFG" e "Mode" da configuração das GPIOs.
 *		   Pin_Hardw	
 *   CFG     MODE    H	 L   
 * |31|30|  |29|28| 15 ; 7		
 * |27|26|  |25|24| 14 ; 6	
 * |23|22|  |21|20| 13 ; 5
 * |19|18|  |17|16| 12 ; 4
 * |15|14|  |13|12| 11 ; 3
 * |11|10|  | 9| 8| 10 ; 2
 * | 7| 6|  | 5| 4|  9 ; 1
 * | 3| 2|  | 1| 0|  8 ; 0
 *
 *
 * __________________________________________
 * HexaCode -> 50MHz
 * 0x03 | 0| 0|  |01..11| <- Out Push-pull
 * 0x07 | 0| 1|  |01..11| <- Out Open-drain
 * 0x0B | 1| 0|  |01..11| <- Alt Out *pull
 * 0x0B | 1| 0|  |01..11| <- Alt Out *drain
 * __________________________________________
 * 0x00 | 0| 0|  |  00  | -< Input Analogig
 * 0x04 | 0| 1|  |  00  | -< Input Floating  
 * 0x08 | 1| 0|  |  00  | -< Input Pull-{down,up}
 * __________________________________________
 * */
